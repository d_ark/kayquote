
Rails.application.routes.draw do


  devise_for :users

  authenticated :user do

    resources :comments
    resources :skills
    resources :workers
    resources :documents

    resources :clients

    resources :tasks do
      post :set_status, on: :member
      post :set_worker, on: :member
      get :documents, on: :member
    end

    resources :languages
    resources :access_rights, only: [:index, :update, :destroy]

    get '/match', to: 'home#match'
    post '/match/results', to: 'home#match_results'
  end

  root 'home#index'

end
