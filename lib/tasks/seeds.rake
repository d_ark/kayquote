namespace :seeds do
  desc "setup users"
  task :users => :environment do
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.clientmanager@gmail.com', client_manager: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.profreader@gmail.com', profreader: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.projectmanager@gmail.com', project_manager: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.admin@gmail.com', admin: true

    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.translator1.prohorova@gmail.com', translator: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.translator2.degtaryov@gmail.com', translator: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.translator3.lazarev@gmail.com', translator: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.translator4.rogova@gmail.com', translator: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.translator5.kovalyova@gmail.com', translator: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.translator6.ponomaryov@gmail.com', translator: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.translator7.volodin@gmail.com', translator: true
    User.create! password: '123', password_confirmation: '123', email: 'antonpriadko+kayquote.translator8.voronova@gmail.com', translator: true
  end

  desc "load languages"
  task :languages => :environment do
    Language.create! name: "Русский"
    Language.create! name: "Украинский"
    Language.create! name: "Английский"
    Language.create! name: "Французкий"
    Language.create! name: "Немецкий"
    Language.create! name: "Итальянский"
    Language.create! name: "Испанский"
    Language.create! name: "Португальский"
    Language.create! name: "Норвежский"
    Language.create! name: "Китайский"
    Language.create! name: "Японский"
    Language.create! name: "Корейский"
  end

  desc "workers list for first sample"
  task :data_first => :environment do
    ActionMailer::Base.delivery_method = :test

    Worker.create! price: 56, user_id: User.find_by(email: 'antonpriadko+kayquote.translator1.prohorova@gmail.com').id, name: 'Татьяна Прохорова'
    Worker.create! price: 62, user_id: User.find_by(email: 'antonpriadko+kayquote.translator2.degtaryov@gmail.com').id, name: 'Илья Дегтярёв'
    Worker.create! price: 54, user_id: User.find_by(email: 'antonpriadko+kayquote.translator3.lazarev@gmail.com').id, name: 'Лука Лазарев'
    Worker.create! price: 63, user_id: User.find_by(email: 'antonpriadko+kayquote.translator4.rogova@gmail.com').id, name: 'Нина Рогова'
    Worker.create! price: 58, user_id: User.find_by(email: 'antonpriadko+kayquote.translator5.kovalyova@gmail.com').id, name: 'Регина Ковалёва'
    Worker.create! price: 58, user_id: User.find_by(email: 'antonpriadko+kayquote.translator6.ponomaryov@gmail.com').id, name: 'Архип Пономарёв'
    Worker.create! price: 62, user_id: User.find_by(email: 'antonpriadko+kayquote.translator7.volodin@gmail.com').id, name: 'Роберт Володин'
    Worker.create! price: 51, user_id: User.find_by(email: 'antonpriadko+kayquote.translator8.voronova@gmail.com').id, name: 'Клара Воронова'

    Client.create! phone: '+380 (95) 258-69-71', email: 'antonpriadko+kayquote.client1.sharov@gmail.com', name: 'Лаврентий Шаров'
    Client.create! phone: '+380 (66) 55-11-589', email: 'antonpriadko+kayquote.client2.ermakova@gmail.com', name: 'Клеопатра Ермакова'
    Client.create! phone: '+380 (63) 248-78-96', email: 'antonpriadko+kayquote.client3.burov@gmail.com', name: 'Аристарх Буров'
    Client.create! phone: '+380 (99) 878-789-8', email: 'antonpriadko+kayquote.client4.golubeva@gmail.com', name: 'Светослава Голубева'
    Client.create! phone: '+380 (44) 785-86-45', email: 'antonpriadko+kayquote.client5.dyachkov@gmail.com', name: 'Казимир Дьячков'
    Client.create! phone: '+380 (93) 4444-888',  email: 'antonpriadko+kayquote.client6.zhukova@gmail.com', name: 'Афродита Жукова'
    Client.create! phone: '+380 (97) 878-78-96', email: 'antonpriadko+kayquote.client7.alexeev@gmail.com', name: 'Аполлон Алексеев'

    languages = Language.all.pluck :id
    workers = Worker.all.pluck :id
    clients = Client.all.pluck :id

    Skill.create! worker_id: workers[0], language_id: languages[2], speed: 433, quality: 9
    Skill.create! worker_id: workers[0], language_id: languages[7], speed: 424, quality: 6
    Skill.create! worker_id: workers[0], language_id: languages[8], speed: 422, quality: 5
    Skill.create! worker_id: workers[0], language_id: languages[5], speed: 354, quality: 6
    Skill.create! worker_id: workers[0], language_id: languages[3], speed: 404, quality: 7
    Skill.create! worker_id: workers[0], language_id: languages[1], speed: 318, quality: 8
    Skill.create! worker_id: workers[0], language_id: languages[4], speed: 481, quality: 4
    Skill.create! worker_id: workers[1], language_id: languages[2], speed: 334, quality: 6
    Skill.create! worker_id: workers[1], language_id: languages[4], speed: 583, quality: 8
    Skill.create! worker_id: workers[1], language_id: languages[0], speed: 366, quality: 9
    Skill.create! worker_id: workers[1], language_id: languages[6], speed: 456, quality: 7
    Skill.create! worker_id: workers[1], language_id: languages[3], speed: 537, quality: 7
    Skill.create! worker_id: workers[1], language_id: languages[8], speed: 475, quality: 5
    Skill.create! worker_id: workers[1], language_id: languages[7], speed: 548, quality: 1
    Skill.create! worker_id: workers[1], language_id: languages[1], speed: 325, quality: 9
    Skill.create! worker_id: workers[2], language_id: languages[4], speed: 587, quality: 8
    Skill.create! worker_id: workers[2], language_id: languages[8], speed: 418, quality: 5
    Skill.create! worker_id: workers[2], language_id: languages[6], speed: 502, quality: 7
    Skill.create! worker_id: workers[2], language_id: languages[0], speed: 498, quality: 8
    Skill.create! worker_id: workers[2], language_id: languages[3], speed: 590, quality: 8
    Skill.create! worker_id: workers[2], language_id: languages[2], speed: 550, quality: 9
    Skill.create! worker_id: workers[2], language_id: languages[5], speed: 365, quality: 9
    Skill.create! worker_id: workers[2], language_id: languages[1], speed: 457, quality: 7
    Skill.create! worker_id: workers[3], language_id: languages[4], speed: 437, quality: 9
    Skill.create! worker_id: workers[3], language_id: languages[8], speed: 400, quality: 6
    Skill.create! worker_id: workers[3], language_id: languages[5], speed: 586, quality: 9
    Skill.create! worker_id: workers[3], language_id: languages[2], speed: 326, quality: 5
    Skill.create! worker_id: workers[3], language_id: languages[3], speed: 426, quality: 4
    Skill.create! worker_id: workers[3], language_id: languages[6], speed: 307, quality: 9
    Skill.create! worker_id: workers[3], language_id: languages[7], speed: 367, quality: 6
    Skill.create! worker_id: workers[4], language_id: languages[5], speed: 318, quality: 9
    Skill.create! worker_id: workers[4], language_id: languages[1], speed: 332, quality: 6
    Skill.create! worker_id: workers[4], language_id: languages[0], speed: 403, quality: 7
    Skill.create! worker_id: workers[4], language_id: languages[7], speed: 440, quality: 4
    Skill.create! worker_id: workers[4], language_id: languages[3], speed: 538, quality: 3
    Skill.create! worker_id: workers[4], language_id: languages[6], speed: 515, quality: 8
    Skill.create! worker_id: workers[4], language_id: languages[4], speed: 554, quality: 7
    Skill.create! worker_id: workers[5], language_id: languages[1], speed: 301, quality: 9
    Skill.create! worker_id: workers[5], language_id: languages[7], speed: 565, quality: 8
    Skill.create! worker_id: workers[5], language_id: languages[4], speed: 325, quality: 6
    Skill.create! worker_id: workers[5], language_id: languages[3], speed: 442, quality: 8
    Skill.create! worker_id: workers[5], language_id: languages[8], speed: 320, quality: 8
    Skill.create! worker_id: workers[5], language_id: languages[0], speed: 370, quality: 9
    Skill.create! worker_id: workers[5], language_id: languages[6], speed: 355, quality: 7
    Skill.create! worker_id: workers[6], language_id: languages[1], speed: 477, quality: 5
    Skill.create! worker_id: workers[6], language_id: languages[3], speed: 302, quality: 8
    Skill.create! worker_id: workers[6], language_id: languages[2], speed: 434, quality: 8
    Skill.create! worker_id: workers[6], language_id: languages[6], speed: 335, quality: 7
    Skill.create! worker_id: workers[6], language_id: languages[5], speed: 352, quality: 7
    Skill.create! worker_id: workers[6], language_id: languages[7], speed: 398, quality: 6
    Skill.create! worker_id: workers[6], language_id: languages[4], speed: 463, quality: 9
    Skill.create! worker_id: workers[6], language_id: languages[0], speed: 499, quality: 5
    Skill.create! worker_id: workers[7], language_id: languages[3], speed: 422, quality: 1
    Skill.create! worker_id: workers[7], language_id: languages[1], speed: 377, quality: 8
    Skill.create! worker_id: workers[7], language_id: languages[7], speed: 597, quality: 7
    Skill.create! worker_id: workers[7], language_id: languages[8], speed: 526, quality: 7
    Skill.create! worker_id: workers[7], language_id: languages[5], speed: 485, quality: 9
    Skill.create! worker_id: workers[7], language_id: languages[0], speed: 445, quality: 7
    Skill.create! worker_id: workers[7], language_id: languages[2], speed: 507, quality: 5

    Task.create! client_id: clients[5], status: :new, from_lang_id: languages[8], to_lang_id: languages[4], deadline: Time.now + 44.days, price: 350, quality: 6, size: 2500, description: 'Перевод документов'
    Task.create! client_id: clients[2], status: :new, from_lang_id: languages[0], to_lang_id: languages[7], deadline: Time.now + 36.days, price: 1100, quality: 6, size: 6500, description: 'Заказ от Василия Федоровича'
    Task.create! client_id: clients[4], status: :new, from_lang_id: languages[4], to_lang_id: languages[1], deadline: Time.now + 16.days, price: 280, quality: 5, size: 1600, description: 'Статья для конференции'
    Task.create! client_id: clients[5], status: :new, from_lang_id: languages[5], to_lang_id: languages[3], deadline: Time.now + 18.days, price: 350, quality: 5, size: 2700, description: 'Пояснительная записка'
    Task.create! client_id: clients[0], status: :new, from_lang_id: languages[0], to_lang_id: languages[6], deadline: Time.now + 37.days, price: 820, quality: 1, size: 5400, description: 'Статья для конференции'
    Task.create! client_id: clients[1], status: :new, from_lang_id: languages[1], to_lang_id: languages[0], deadline: Time.now + 47.days, price: 400, quality: 2, size: 2500, description: 'Пакет документов'
    Task.create! client_id: clients[3], status: :new, from_lang_id: languages[1], to_lang_id: languages[3], deadline: Time.now + 36.days, price: 280, quality: 5, size: 1300, description: 'Техническая документация'
    Task.create! client_id: clients[2], status: :new, from_lang_id: languages[2], to_lang_id: languages[7], deadline: Time.now + 33.days, price: 500, quality: 6, size: 3300, description: 'Что-то страшное и непонятное'
    Task.create! client_id: clients[1], status: :new, from_lang_id: languages[0], to_lang_id: languages[1], deadline: Time.now + 33.days, price: 390, quality: 3, size: 2500, description: 'Документы'
    Task.create! client_id: clients[6], status: :new, from_lang_id: languages[7], to_lang_id: languages[3], deadline: Time.now + 39.days, price: 630, quality: 6, size: 4300, description: 'Художественный перевод'
    Task.create! client_id: clients[4], status: :new, from_lang_id: languages[2], to_lang_id: languages[0], deadline: Time.now + 22.days, price: 750, quality: 5, size: 5200, description: 'Документы'
    Task.create! client_id: clients[6], status: :new, from_lang_id: languages[7], to_lang_id: languages[8], deadline: Time.now + 50.days, price: 1020, quality: 3, size: 7000, description: 'Перевод документов'
    Task.create! client_id: clients[3], status: :ready, worker_id: 6, from_lang_id: languages[1], to_lang_id: languages[0], deadline: Time.now + 50.days, price: 20000, quality: 4, size: 32000, description: 'Перевод дипломной работы'


    Comment.create! source: "client_manager", worker_id: nil,  task_id: 13, text: "Заказчик просил обязательно учесть особенности технического языка - во вложении есть файл с переведенной терминологией, можно при необходимости брать данные оттуда."
    Comment.create! source: "client_manager", worker_id: nil,  task_id: 13, text: "Еще момент - главу 8 и 9 переводить не нужно!"
    Comment.create! source: "translator",     worker_id: 6,    task_id: 13, text: "Не понятно. Тоесть главу 8 и 9 оставить на Украинском? А как я тогда вставлю их в переведенный документ?"
    Comment.create! source: "translator",     worker_id: 6,    task_id: 13, text: "В общем я в результат просто не добавляю эти главы, а там уже клиент пусть разбирается сам..."
    Comment.create! source: "profreader",     worker_id: nil,  task_id: 13, text: "В 4 главе не корректный перевод абзаца про технические характеристики - используй терминологию из приложенного файла"
    Comment.create! source: "translator",     worker_id: 6,    task_id: 13, text: "В файле не все нашел. Но вроде сейчас должно быть правильно, я в других источних нашел..."
    Comment.create! source: "profreader",     worker_id: nil,  task_id: 13, text: "Так лучше, спасибо. Больше претензий нет"
    Comment.create! source: "client_manager", worker_id: nil,  task_id: 13, text: "Показала клиентке, ей не понравился перевод в 11й главе. Сказала, что нужно более научным языком изложить. Переделай, пожалуйста =)"
    Comment.create! source: "translator",     worker_id: 6,    task_id: 13, text: "Научным так научным, нет проблем =)"


    Document.first
    Object.send(:remove_const, :Document)
    class Document < ActiveRecord::Base
    end

    Document.create! task_id: 13, kind: :source, document: 'terminology.pdf'
    Document.create! task_id: 13, kind: :source, document: 'Diplom.docx'
    Document.create! task_id: 13, kind: :result, document: 'diplom-translated.docx'
  end

  desc "preload"
  task :preload => [:users, :languages]

  desc "load first sample"
  task :first => [:preload, :data_first]
end
