module Matcher
  class Hungarian
    EMPTY = 0
    STAR  = 1
    PRIME = 2

    def initialize(matrix = nil)
      setup(matrix) if matrix
    end

    def solve(matrix = nil)
      setup(matrix) if matrix

      method = :minimize_rows
      while method != :finished
        method = self.send(*method)
      end

      return assignment
    end

    private

    def minimize_rows
      @matrix.map! do |row|
        min_value = row.min
        row.map { |element| element - min_value }
      end

      return :star_zeroes
    end

    def star_zeroes
      traverse_indices do |row, column|
        if @matrix[row][column].zero? && !location_covered?(row, column)
          @mask[row][column] = STAR
          cover_cell(row, column)
        end
      end
      reset_covered_hash

      return :mask_columns
    end

    def mask_columns
      index_range.each do |index|
        @covered[:columns][index] = true if column_mask_values_for(index).any? { |value| value == STAR }
      end

      return @covered[:columns].all? ? :finished : :prime_zeroes
    end

    def prime_zeroes
      while (row, column = find_uncovered_zero) != [-1, -1]
        @mask[row][column] = PRIME

        if star_loc_in_row = row_mask_values_for(row).index(STAR)
          @covered[:rows][row] = true
          @covered[:columns][star_loc_in_row] = false
        else
          return :augment_path, row, column
        end
      end

      return :adjust_matrix
    end

    def augment_path(starting_row, starting_column)
      path = [[starting_row, starting_column]]
      path.instance_eval do
        def previous_row;    self.last[0]; end
        def previous_column; self.last[1]; end
      end

      loop do
        if row_containing_star = column_mask_values_for(path.previous_column).index(STAR)
          path << [row_containing_star, path.previous_column]
        else
          break
        end

        col_containing_prime = row_mask_values_for(path.previous_row).index(PRIME)
        path << [path.previous_row, col_containing_prime]
      end

      update_elements_in(path)
      traverse_indices { |row, column| @mask[row][column] = EMPTY if @mask[row][column] == PRIME }
      reset_covered_hash

      return :mask_columns
    end

    def adjust_matrix
      smallest_value = nil
      traverse_indices do |row, column|
        if !location_covered?(row, column) && (smallest_value.nil? || @matrix[row][column] < smallest_value)
          smallest_value = @matrix[row][column]
        end
      end

      indices_of_covered_rows.each { |index| @matrix[index].map! { |value| value + smallest_value } }

      covered_columns = indices_of_uncovered_columns
      index_range.each { |row| covered_columns.each { |column| @matrix[row][column] -= smallest_value } }

      return :prime_zeroes
    end

    def setup(matrix)
      @matrix  = matrix
      @length  = @matrix.length
      @mask    = Array.new(@length) { Array.new(@length, EMPTY) }
      @covered = { :rows => Array.new(@length, false), :columns => Array.new(@length, false) }
    end

    def assignment
      index_range.inject([]) { |path, row_index| path << [row_index, @mask[row_index].index(STAR)] }
    end

    def update_elements_in(path)
      path.each do |cell|
        @mask[cell[0]][cell[1]] = case @mask[cell[0]][cell[1]]
        when STAR  then EMPTY
        when PRIME then STAR
        end
      end
    end

    def find_uncovered_zero
      traverse_indices do |row, column|
        return [row, column] if @matrix[row][column].zero? && !location_covered?(row, column)
      end
      [-1, -1]
    end

    def cover_cell(row, column)
      @covered[:rows][row] = @covered[:columns][column] = true
    end

    def reset_covered_hash
      @covered.values.each { |cover| cover.fill(false) }
    end

    def location_covered?(row, column)
      @covered[:rows][row] || @covered[:columns][column]
    end

    def row_mask_values_for(row)
      index_range.map { |column| @mask[row][column] }
    end

    def column_mask_values_for(column)
      index_range.map { |row| @mask[row][column] }
    end

    def indices_of_covered_rows
      index_range.select { |index| @covered[:rows][index] }
    end

    def indices_of_uncovered_columns
      index_range.select { |index| !@covered[:columns][index] }
    end

    def traverse_indices(&block)
      index_range.each { |row| index_range.each { |column| yield row, column } }
    end

    def index_range
      (0...@length)
    end
  end


  def self.perform workers, tasks, locked, ignored_tasks, ignored_workers
      graph = build_graph workers, tasks, locked, ignored_tasks, ignored_workers
      matrix = graph.matrix

      hungarian = Hungarian.new(matrix)
      pairs = hungarian.solve

      pairs = pairs.map do |pair|
        if workers[pair[0]].try(:accepts?, tasks[pair[1]]).try(:result) && !ignored_workers.include?(workers[pair[0]].id.to_s) && !ignored_tasks.include?(tasks[pair[1]].id.to_s)
          graph.detail_data[pair[0]].good.each do |task|
            task.locked = (task.task == tasks[pair[1]]) && (locked[task.task.id.to_s] == workers[pair[0]].id.to_s)
            task.choosen = (task.task == tasks[pair[1]]) && !task.locked
          end
          Hashie::Mash.new task: tasks[pair[1]], worker: workers[pair[0]]
        end
      end

      pairs = pairs.compact

      recieved = 0
      payed = 0

      pairs.each do |pair|
        recieved += pair.task.price
        payed += pair.worker.costs(pair.task) || 0
      end

      Hashie::Mash.new matched: pairs.count,
                       rejected: tasks.count - pairs.count,
                       free_workers: workers.count - pairs.count,
                       recieved: recieved,
                       payed: payed,
                       pairs: pairs,
                       workers: graph.detail_data
  end

  def self.build_graph workers, tasks, locked, ignored_tasks, ignored_workers
    size = [workers.count, tasks.count].max

    matrix = []
    detail_data = []

    maximum = 0

    size.times do |i|
      row = []
      detail_row = Hashie::Mash.new good: [], bad: [], worker: workers[i]
      size.times do |j|
        accepts = workers[i].try(:accepts?, tasks[j])

        if workers[i] && tasks[j]
          detail_row.good.push accepts if accepts.result
          detail_row.bad.push accepts unless accepts.result
        end

        if accepts.try(:result)
          s = [0, tasks[j].price - workers[i].costs(tasks[j])].max
          s = (locked[tasks[j].id.to_s] == workers[i].id.to_s) ? 10000000000 : 0 if locked[tasks[j].id.to_s]
          s = 0 if ignored_workers.include? workers[i].id.to_s
          s = 0 if ignored_tasks.include? tasks[j].id.to_s
          row[j] = s
          maximum = s if maximum < s
        else
          row[j] = 0
        end
      end
      detail_data[i] = detail_row if workers[i]
      matrix[i] = row
    end

    size.times do |i|
      size.times do |j|
        matrix[i][j] = maximum - matrix[i][j]
      end
    end
    Hashie::Mash.new matrix: matrix, detail_data: detail_data
  end
end
