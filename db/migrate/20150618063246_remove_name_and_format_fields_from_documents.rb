class RemoveNameAndFormatFieldsFromDocuments < ActiveRecord::Migration
  def change
    remove_column :documents, :name, :string
    remove_column :documents, :format, :string
  end
end
