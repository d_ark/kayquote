class AddFieldStatusToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :status, :string, null: false, default: 'new'
  end
end
