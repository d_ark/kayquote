class AddFieldUserToWorker < ActiveRecord::Migration
  def change
    add_reference :workers, :user, index: true, foreign_key: true
  end
end
