class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.integer :quality
      t.integer :speed
      t.references :language, index: true
      t.references :worker, index: true

      t.timestamps
    end
  end
end
