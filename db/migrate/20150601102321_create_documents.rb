class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.references :task, index: true, foreign_key: true
      t.string :kind
      t.string :name
      t.string :format

      t.timestamps null: false
    end
  end
end
