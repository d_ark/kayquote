class AddFieldTranslatorToUserModel < ActiveRecord::Migration
  def change
    add_column :users, :translator, :boolean, default: false, null: false
  end
end
