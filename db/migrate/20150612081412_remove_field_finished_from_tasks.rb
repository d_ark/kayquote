class RemoveFieldFinishedFromTasks < ActiveRecord::Migration
  def change
    remove_column :tasks, :finished, :boolean, null: false, default: false
  end
end
