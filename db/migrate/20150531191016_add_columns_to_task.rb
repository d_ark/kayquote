class AddColumnsToTask < ActiveRecord::Migration
  def change
    add_reference :tasks, :client, index: true, foreign_key: true
    add_column :tasks, :status, :string
  end
end
