class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :to_lang, index: true
      t.references :from_lang, index: true
      t.date :deadline
      t.float :price
      t.integer :quality
      t.integer :size
      t.boolean :finished, null: false, default: false

      t.timestamps
    end
  end
end
