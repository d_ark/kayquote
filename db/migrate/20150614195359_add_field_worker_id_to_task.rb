class AddFieldWorkerIdToTask < ActiveRecord::Migration
  def change
    add_reference :tasks, :worker, index: true, foreign_key: true
  end
end
