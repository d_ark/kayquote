# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150618063246) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string   "text"
    t.string   "source"
    t.integer  "worker_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["task_id"], name: "index_comments_on_task_id", using: :btree
  add_index "comments", ["worker_id"], name: "index_comments_on_worker_id", using: :btree

  create_table "documents", force: :cascade do |t|
    t.integer  "task_id"
    t.string   "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "document"
  end

  add_index "documents", ["task_id"], name: "index_documents_on_task_id", using: :btree

  create_table "languages", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "skills", force: :cascade do |t|
    t.integer  "quality"
    t.integer  "speed"
    t.integer  "language_id"
    t.integer  "worker_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "skills", ["language_id"], name: "index_skills_on_language_id", using: :btree
  add_index "skills", ["worker_id"], name: "index_skills_on_worker_id", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.integer  "to_lang_id"
    t.integer  "from_lang_id"
    t.date     "deadline"
    t.float    "price"
    t.integer  "quality"
    t.integer  "size"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.integer  "client_id"
    t.string   "status",       default: "new", null: false
    t.integer  "worker_id"
  end

  add_index "tasks", ["client_id"], name: "index_tasks_on_client_id", using: :btree
  add_index "tasks", ["from_lang_id"], name: "index_tasks_on_from_lang_id", using: :btree
  add_index "tasks", ["to_lang_id"], name: "index_tasks_on_to_lang_id", using: :btree
  add_index "tasks", ["worker_id"], name: "index_tasks_on_worker_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "project_manager",        default: false, null: false
    t.boolean  "client_manager",         default: false, null: false
    t.boolean  "profreader",             default: false, null: false
    t.boolean  "admin",                  default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "translator",             default: false, null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "workers", force: :cascade do |t|
    t.string   "name"
    t.float    "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "workers", ["user_id"], name: "index_workers_on_user_id", using: :btree

  add_foreign_key "comments", "tasks"
  add_foreign_key "comments", "workers"
  add_foreign_key "documents", "tasks"
  add_foreign_key "tasks", "clients"
  add_foreign_key "tasks", "workers"
  add_foreign_key "workers", "users"
end
