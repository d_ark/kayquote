
workers = 8
languages = 9
languages_count_min = 6
languages_count_max = 8
tasks = 12

def rand_speed
  Random.rand(300) + 300
end

def rand_quality
  s = [1,2,3,4,4,5,5,5,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,10]
  s[Random.rand(s.size)]
end


randoms = ([0]*8).map do
  count = languages_count_min + Random.rand(languages_count_max - languages_count_min)
  (0..languages - 1).to_a.shuffle[0..count]
end


s = ''
randoms.each_with_index do |row, i|
  row.each do |j|
    s += "Skill.create! worker_id: workers[#{i}], language_id: languages[#{j}], speed: #{rand_speed}, quality: #{rand_quality}\n"
  end
end

languages = 9

12.times do
  ids = (0..languages - 1).to_a.shuffle[0..1]
  days = 14 + Random.rand(40)
  size = 100 * (5 + Random.rand(80))

  s += "Task.create! from_lang_id: languages[#{ids[0]}], to_lang_id: languages[#{ids[1]}], deadline: Time.now + #{days}.days, price: #{size * 60 * 1.1 / 450}, quality: #{1 + Random.rand(6)}, size: #{size}\n"
end

puts s
