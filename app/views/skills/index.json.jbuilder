json.array!(@skills) do |skill|
  json.extract! skill, :id, :quality, :speed, :language_id, :worker_id
  json.url skill_url(skill, format: :json)
end
