json.array!(@tasks) do |task|
  json.extract! task, :id, :to_lang_id, :from_lang_id, :deadline, :price, :quality, :size, :finished
  json.url task_url(task, format: :json)
end
