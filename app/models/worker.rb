class Worker < ActiveRecord::Base
  scope :sorted, -> {order :name}
  scope :free, -> {where(id: all.pluck(:id) - Task.where(status: [:assigned, :working, :finished, :rejected]).pluck(:worker_id))}
  has_many :skills
  has_many :tasks

  validates_presence_of :name
  validates_numericality_of :price, greater_than: 0

  belongs_to :user

  def time_spent_on task
    to_speed = skills.where(language_id: task.to_lang_id).take.try :speed
    from_speed = skills.where(language_id: task.from_lang_id).take.try :speed


    if (to_speed && from_speed)
      (task.size / [from_speed, to_speed].min).ceil
    else
      nil
    end
  end

  def costs task
    price * time_spent_on(task)
  end

  def accepts? task
    to_skill = skills.where(language_id: task.to_lang_id).take
    from_skill = skills.where(language_id: task.from_lang_id).take

    return Hashie::Mash.new({task: task, result: false, description: "not_skilled_in", skill: task.from_lang.name}) unless from_skill
    return Hashie::Mash.new({task: task, result: false, description: "not_skilled_in", skill: task.to_lang.name}) unless to_skill
    return Hashie::Mash.new({task: task, result: false, description: "not_skilled_enough", skill: task.from_lang.name}) if from_skill.quality < task.quality
    return Hashie::Mash.new({task: task, result: false, description: "not_skilled_enough", skill: task.to_lang.name}) if to_skill.quality < task.quality
    return Hashie::Mash.new({task: task, result: false, description: "deadline"}) if time_spent_on(task).business_hours.from_now >= task.deadline
    cost = costs task
    return Hashie::Mash.new({task: task, result: false, description: "economicaly_bad", income: task.price, expense: cost}) if cost > task.price
    Hashie::Mash.new({task: task, result: true, profit: task.price - cost})
  end
end
