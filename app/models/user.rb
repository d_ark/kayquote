class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  has_one :worker

  devise :database_authenticatable, :rememberable, :registerable, :validatable, :recoverable

  ALLOWED_PERMISSIONS = ["project_manager", "client_manager", "profreader", "admin"]

  def set_permission permission
    val = permission.starts_with? '+'
    code = permission[1..permission.size]
    return unless ALLOWED_PERMISSIONS.include? code

    update! code => val
  end


  def project_manager?
    project_manager
  end

  def client_manager?
    client_manager
  end

  def profreader?
    profreader
  end

  def admin?
    admin
  end

  def translator?
    translator
  end

  def not_granted?
    !admin && !profreader && !client_manager && !project_manager && !translator
  end

end
