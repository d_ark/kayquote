class Task < ActiveRecord::Base
  belongs_to :to_lang, class_name: :Language
  belongs_to :from_lang, class_name: :Language
  belongs_to :client
  belongs_to :worker
  has_many :documents
  has_many :comments

  scope :sorted, -> {order :id}
  scope :active, -> {where status: [:new, :assigned, :working, :finished, :ready, :rejected]}
  scope :unassigned, -> {where status: :new}
  scope :assigned, -> {where status: :assigned}
  scope :working, -> {where status: :working}
  scope :finished, -> {where status: :finished}
  scope :ready, -> {where status: :ready}
  scope :rejected, -> {where status: :rejected}
  scope :closed, -> {where status: :closed}

  ALLOWED_STATUSES = [:new, :assigned, :working, :finished, :ready, :rejected, :canceled, :closed]

  validates_presence_of :to_lang, :from_lang, :deadline, :price, :quality, :size
  validates_numericality_of :price, greater_than: 0
  validates_numericality_of :quality,
                            only_integer: true,
                            less_than_or_equal_to: 10,
                            greater_than_or_equal_to: 1
  validates_numericality_of :size,
                            only_integer: true,
                            greater_than: 0
  validate :languages_are_different

  before_save :check_status

  after_create :send_project_manager_message_on_create
  after_save :send_assigned_message
  after_save :send_finished_message
  after_save :send_rejected_message
  after_save :send_client_message_on_ready

  def check_status
    if status.to_sym == :new && worker
      self.status = :assigned
    end
    if status.to_sym == :assigned && !worker
      self.status = :new
    end
  end

  def languages_are_different
    if to_lang.id == from_lang.id
      errors.add :from_lang, ''
      errors.add :to_lang, ''
    end
  end

  def send_project_manager_message_on_create
    ClientMailer.new_to_projectmanager(self).deliver_now
  end

  def send_assigned_message
    ClientMailer.assigned_to_translator(self).deliver_now if status_changed? && status.to_sym == :assigned
  end

  def send_finished_message
    ClientMailer.finished_to_profreader(self).deliver_now if status_changed? && status.to_sym == :finished
  end

  def send_rejected_message
    ClientMailer.rejected_to_translator(self).deliver_now if status_changed? && status.to_sym == :rejected
  end

  def send_client_message_on_ready
    if status_changed? && status.to_sym == :ready
      ClientMailer.ready(self).deliver_now
      ClientMailer.ready_to_clientmanager(self).deliver_now
    end
  end
end
