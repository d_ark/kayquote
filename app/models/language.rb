class Language < ActiveRecord::Base
  has_many :skills
  has_many :tasks, foreign_key: :from_lang_id
  has_many :tasks, foreign_key: :to_lang_id

  def can_destroy?
    tasks.empty? && skills.empty?
  end
end
