class Skill < ActiveRecord::Base
  belongs_to :language
  belongs_to :worker

  validates_presence_of :worker, :language, :speed, :quality
  validates_numericality_of :speed,
                            only_integer: true,
                            greater_than: 0
  validates_numericality_of :quality,
                            only_integer: true,
                            less_than_or_equal_to: 10,
                            greater_than_or_equal_to: 1
  validate :languages_are_different

  def languages_are_different
    same_skills = worker.skills.where(language_id: language_id)
    if same_skills.count > 1 || (new_record? && same_skills.count == 1)
      errors.add :from_lang, ''
      errors.add :to_lang, ''
    end
  end

end
