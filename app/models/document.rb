class Document < ActiveRecord::Base
  belongs_to :task
  mount_uploader :document, DocumentUploader

  scope :result, -> { where kind: :result }
  scope :source, -> { where kind: :source }


  def format
    return 'doc' if document.url.ends_with? 'doc'
    return 'doc' if document.url.ends_with? 'docx'
    return 'pdf' if document.url.ends_with? 'pdf'
    return 'jpg' if document.url.ends_with? 'jpg'
    return 'jpg' if document.url.ends_with? 'png'
    return 'jpg' if document.url.ends_with? 'jpeg'
    return 'jpg' if document.url.ends_with? 'tif'
    return 'jpg' if document.url.ends_with? 'tiff'
    return 'jpg' if document.url.ends_with? 'gif'
    return 'jpg' if document.url.ends_with? 'svg'
    return 'jpg' if document.url.ends_with? 'bmp'
    'unknown'
  end

  def name
    document.url.split('/').last
  end
end
