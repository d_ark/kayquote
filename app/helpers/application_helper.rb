module ApplicationHelper
  def link_to_unless_with_block condition, uri, *args, &block
    link_to_unless condition, capture(&block), uri, *args
  end
end
