module HomeHelper
  def locked_without_pair task_id, worker_id
    Hash[
      @locked.select {|k, v| k.to_s != task_id.to_s && v.to_s != worker_id.to_s}
    ]
  end

  def with_fixed_pair task_id, worker_id
    params.merge(
      locked: locked_without_pair(task_id, worker_id).merge(task_id.to_s => worker_id),
      ignored_workers: (@ignored_workers - [worker_id.to_s]).sort.uniq,
      ignored_tasks: (@ignored_tasks - [task_id.to_s]).sort.uniq
    )
  end

  def without_fixed_pair task_id, worker_id
    params.merge(
      locked: locked_without_pair(task_id, worker_id)
    )
  end

  def without_worker id
    params.merge(
      ignored_workers: (@ignored_workers + [id.to_s]).sort.uniq,
      locked: locked_without_pair(nil, id)
    )
  end

  def with_worker id
    params.merge(
      ignored_workers: (@ignored_workers - [id.to_s]).sort.uniq
    )
  end

  def without_task id
    params.merge(
      ignored_tasks: (@ignored_tasks + [id.to_s]).sort.uniq,
      locked: locked_without_pair(id, nil)
    )
  end

  def with_task id
    params.merge(
      ignored_tasks: (@ignored_tasks - [id.to_s]).sort.uniq
    )
  end
end
