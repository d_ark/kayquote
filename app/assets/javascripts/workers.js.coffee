# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

@setupSkillForms = ->
  ($ 'table#skill-list-table td.no-form').on 'click', (ev)->
    ($ 'table#skill-list-table td.form').addClass 'hide'
    ($ 'table#skill-list-table td.no-form').removeClass 'hide'

    id = ev.currentTarget.dataset.skillId

    ($ "table#skill-list-table td.form[data-skill-id=#{id}]").removeClass 'hide'
    ($ "table#skill-list-table td.no-form[data-skill-id=#{id}]").addClass 'hide'
