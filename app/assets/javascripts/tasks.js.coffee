# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

@setupTasksForm = ->
  $ ->
    ($ '.datepicker').datepicker
      dateFormat: "dd.mm.yy"
    ($ '.field_with_errors').each (i, x) ->
      ($ ($ x).parent()).addClass 'error'


# @addFilesTo = (where)->
#   $where = $ "##{where}"

$(document).on 'change', '.file_submiter',  ->
  $(this).closest('form').submit()
