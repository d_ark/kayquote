$ ->
  ($ document).foundation
    abide:
      patterns:
        currency: /^\d*([\.,]\d{0,2})?$/
        mark10: /^(10|[1-9])$/
        integer: /^[1-9]\d*$/
      validators:
        differentTo: (el, required, parent)->
          return el.value isnt ($ "##{el.dataset['differentTo']}").val()
        uniqueThrough: (el, required, parent)->
          val = el.value
          count = 0
          ($ el.dataset.uniqueThrough).each (i, x)->
            unless x == el
              count++ if val == x.value
          count == 0

  currency_format = (value)->
    value = value.replace(',', '.')

  ($ 'form').on 'valid', ->
    ($ this).find('input[pattern=currency]').each ->
      ($ this).val currency_format this.value
