# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


@setupLanguageForms = ->
  ($ '.language_container').on 'click', (ev)->
    target = ev.currentTarget
    ($ ".form_for_language_container").addClass 'hide'
    ($ ".language_container").removeClass 'hide'

    ($ "#form_for_#{target.id}").removeClass 'hide'
    ($ target).addClass 'hide'
