# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


@setupChooseWorkersForm = ->
  ($ '#select_all').on 'click', (ev)->
    ($ 'form input[type=checkbox]').prop 'checked', ev.currentTarget.checked


($ document).on 'click', '.worker-row', (ev) ->
  unless $(ev.target).closest('.td-link').is '*'
    do ($ ".#{ev.currentTarget.id}").toggle
