class ClientMailer < ApplicationMailer
  layout false

  def ready(task)
    @task = task
    mail subject: "Ваш заказ № #{task.id} в KayQuote готов!", to: task.client.email
  end

  def new_to_projectmanager task
    @task = task
    mail subject: "Новый заказ №#{task.id} требует распределения", to: User.where(project_manager: true).map {|x| x.email}
  end

  def assigned_to_translator task
    @task = task
    mail subject: "Вам распределен заказ №#{task.id}", to: task.worker.user.email
  end

  def finished_to_profreader task
    @task = task
    mail subject: "Заказ №#{task.id} завершен и ожидает проверки", to: User.where(profreader: true).map {|x| x.email}
  end

  def rejected_to_translator task
    @task = task
    mail subject: "Заказ №#{task.id} отклонен", to: task.worker.user.email
  end

  def ready_to_clientmanager task
    @task = task
    mail subject: "Заказ №#{task.id} готов для передачи клиенту", to: User.where(client_manager: true).map {|x| x.email}
  end

end
