class WorkersController < ApplicationController
  before_action :control_access

  before_action :set_worker, only: [:show, :edit, :update, :destroy]
  before_action :set_workers, only: [:index]

  # GET /workers
  # GET /workers.json
  def index
    @workers = @workers.where("LOWER(name) LIKE ?", "%#{params[:q].mb_chars.downcase}%") if params[:q]
    @searching = !! params[:q]
    @workers = @workers.page params[:page]
  end

  # GET /workers/1
  # GET /workers/1.json
  def show
  end

  # GET /workers/new
  def new
    @worker = Worker.new
  end

  # GET /workers/1/edit
  def edit
    @skills = @worker.skills
    @skill = Skill.new

    @tasks = Task.where(status: :new)
    @tasks = @tasks.to_a.select {|x| @worker.accepts?(x).result }
    @tasks = @worker.tasks.where(status: :assigned) + Task.where(id: @tasks.map(&:id) - @worker.tasks.where(status: :assigned).map(&:id))
  end

  # POST /workers
  # POST /workers.json
  def create
    @worker = Worker.new(worker_params)

    respond_to do |format|
      if @worker.save
        format.html { redirect_to workers_url }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /workers/1
  # PATCH/PUT /workers/1.json
  def update
    respond_to do |format|
      if @worker.update(worker_params)
        if current_user.translator?
          format.html { redirect_to :root }
        else
          format.html { redirect_to workers_url }
        end
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /workers/1
  # DELETE /workers/1.json
  def destroy
    @worker.destroy
    respond_to do |format|
      format.html { redirect_to workers_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_worker
    @worker = Worker.find(params[:id])
  end

  def set_workers
    @workers = Worker.all.sorted
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def worker_params
    params.require(:worker).permit(:name, :price)
  end

  def control_access
    redirect_to root_path unless current_user.admin? ||
                                 current_user.profreader? ||
                                 current_user.project_manager? ||
                                 current_user.translator?
  end
end
