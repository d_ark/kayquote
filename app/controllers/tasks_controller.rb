require 'zip'

class TasksController < ApplicationController
  before_action :control_access_read
  before_action :control_access_edit, only: [:new, :edit, :create, :update]
  before_action :control_access_delete, only: [:destroy]


  before_action :set_task, only: [:show, :edit, :update, :destroy, :documents]
  before_action :set_tasks, only: [:index]

  # GET /tasks
  # GET /tasks.json
  def index
    if params[:q]
      redirect_to url_for(params.except(:filter)) if params[:filter]
      @special_tasks = []
      if params[:q] == params[:q].to_i.to_s
        @tasks = Task.where(id: params[:q])
      else
        @tasks = Task.where('LOWER(description) LIKE ?', "%#{params[:q].mb_chars.downcase}%")
      end
    elsif params[:filter]
      @special_tasks = []
      @tasks = Task.where(status: params[:filter].keys)
    elsif current_user.client_manager? || current_user.profreader? || current_user.admin? || current_user.project_manager?
      if params[:client_id]
        @tasks = Task.where(client_id: params[:client_id])
        @tasks = @tasks.page params[:page]
      else
        @tasks = Task.all
      end

      @special_tasks = []
      @special_tasks = @tasks.where(status: :ready) if current_user.client_manager?
      @special_tasks = @tasks.where(status: :finished) if current_user.profreader?
      @special_tasks = @tasks.where(status: :new) if current_user.project_manager?

      if @special_tasks.any?
        ids = @tasks.pluck(:id) - @special_tasks.pluck(:id)
        @tasks = Task.where(id: ids)
      end
    else
      @special_tasks = []
      @tasks = Task.where(status: [:assigned, :working, :finished, :rejected], worker_id: current_user.worker.id) if current_user.translator?
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
    if [:new, :assigned].include?(@task.status.to_sym) && (current_user.admin? || current_user.project_manager?)
      @workers = Worker.all
      if params[:q]
        @workers = @workers.where("LOWER(name) LIKE ?", "%#{params[:q].mb_chars.downcase}%")
      end
      @workers = @workers.to_a.select {|x| x.accepts?(@task).result }
      @workers = [@task.worker] + Worker.where(id: @workers.map(&:id) - [@task.worker_id]) if @task.worker
    end
  end

  def set_status
    Task.find(params[:id]).update! set_status_params
    puts edit_task_url(id: params[:id]), '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    redirect_to edit_task_url id: params[:id]
  end

  def set_worker
    @task = Task.find(params[:id])
    if params[:worker_id]
      @task.update! worker_id: Worker.find(params[:worker_id]).id
    else
      @task.update! worker_id: nil
    end
    redirect_to :back
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)

    respond_to do |format|
      if @task.save
        format.html { redirect_to edit_task_url(@task) }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to edit_task_url(@task)}
     else
        format.html { render :edit }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url }
      format.json { head :no_content }
    end
  end


  def documents
    file = Tempfile.new('files.zip')

    names = []

    Zip::File.open(file.path, Zip::File::CREATE) do |zipfile|
      @task.documents.where(kind: params[:kind]).each do |document|
        name = document.name
        if names.include? name
          i = 1
          while names.include? name
            splitted = document.name.split '.'
            if splitted.count == 1
              name = document.name + '-' + i.to_s
            else
              splitted[splitted.count - 2] =  splitted[splitted.count - 2]  + '-' + i.to_s
              name = splitted.join '.'
            end
            i = i + 1
          end
        end
        names << name
        zipfile.add(name, document.document.path)
      end
    end

    send_data File.read(file.path), filename: 'document.zip'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_task
    @task = Task.find(params[:id])
  end

  def set_tasks
    if params[:finished]
      @tasks = Task.where(finished: params[:finished]).sorted
    else
      @tasks = Task.all.sorted
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def task_params
    params.require(:task).permit(:to_lang_id, :from_lang_id, :deadline, :price, :quality, :size, :finished, :description, :status, :client_id)
  end

  def set_status_params
    params.require(:task).permit(:finished)
  end

  def control_access_read
    redirect_to root_path unless current_user.admin? ||
                                 current_user.profreader? ||
                                 current_user.client_manager? ||
                                 current_user.translator? ||
                                 current_user.project_manager?
  end

  def control_access_edit
    true
  end

  def control_access_delete
    redirect_to root_path unless current_user.admin?
  end
end
