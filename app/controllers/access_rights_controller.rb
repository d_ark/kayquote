class AccessRightsController < ApplicationController
  before_filter :control_access

  before_filter :set_user, only: [:destroy, :update]
  before_filter :prevent_editing_self, only: [:destroy, :update]

  def index
    @users = User.all.order :id
  end

  def update
    @user.set_permission params[:permission]
    redirect_to access_rights_path
  end

  def destroy
    @user.destroy!
    redirect_to access_rights_path
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def prevent_editing_self
    redirect_to access_rights_path if @user == current_user
  end

  def control_access
    redirect_to root_path unless current_user.admin?
  end
end
