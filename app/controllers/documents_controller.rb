class DocumentsController < ApplicationController
  def create
    Document.create params.require(:document).permit(:document, :kind, :task_id)
    redirect_to :back
  end

  def index
    render json: Document.all
  end

  def destroy
    Document.find(params[:id]).destroy
    redirect_to :back
  end
end
