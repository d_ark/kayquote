class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  def match
    @locked = params[:locked] || {}
    @ignored_workers = params[:ignored_workers] || []
    @ignored_tasks = params[:ignored_tasks] || []

    @result = Matcher::perform Worker.free, Task.where(status: :new), @locked, @ignored_tasks, @ignored_workers
    @best_result = Matcher::perform Worker.free, Task.where(status: :new), {}, [], []
    @not_best = @locked.any? || @ignored_tasks.any? || @ignored_workers.any?
  end

  def match_results
    JSON.parse(params[:pairs]).each do |x|
      Task.find(x['task_id'].to_i).update! worker_id: Worker.find(x['worker_id'].to_i).id
    end
    redirect_to :root
  end

  private

  def match_params
    params.require :worker_ids
  end
end
