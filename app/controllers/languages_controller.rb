class LanguagesController < ApplicationController
  before_action :control_access, only: [:update, :create, :destroy]
  before_action :set_language, only: [:show, :edit, :update, :destroy]

  # GET /languages
  # GET /languages.json
  def index
    @languages = Language.all
    @language = Language.new
  end

  # POST /languages
  # POST /languages.json
  def create
    @language = Language.new(language_params)

    respond_to do |format|
      if @language.save
        format.html { redirect_to languages_url }
      end
    end
  end

  # PATCH/PUT /languages/1
  # PATCH/PUT /languages/1.json
  def update
    respond_to do |format|
      if @language.update(language_params)
        format.html { redirect_to languages_url }
      end
    end
  end

  # DELETE /languages/1
  # DELETE /languages/1.json
  def destroy
    if @language.can_destroy?
      @error = false
      @language.destroy
    else
      @error = true
    end
    @languages = Language.all
    @language = Language.new
    render :index
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_language
    @language = Language.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def language_params
    params.require(:language).permit(:name)
  end

  def control_access
    redirect_to root_path unless current_user.admin? || current_user.project_manager?
  end
end
